Stupid Debian Tricks
--------------------

I've been doing Debian stuff for a long time now. I'll put here a few things
that make my life easier. Maybe they'll be useful. Suggestions for doing it
better?  File an issue or send a patch request. 😁

I try to make these all a drop-in that shouldn't hurt any settings they aren't
explicitly trying to change. That's for my own sanity, but I encourage
adopting the practice. Remember the rule: Anything you haven't looked at for
six months may as well have been written by someone else.

* etc_default_grub.d/11-nvme-safe-shutdown.cfg
  The Linux kernel tells NVMe devices to shut down, but times out waiting for
  them to acknowledge and then just cuts power, leading to smartmontools
  reporting unsafe shutdown errors. Increase the wait time to 8.

* etc_default_grub.d/zz_timeout.cfg
  Ubuntu hides the GRUB menu by default making it almost impossible to get
  into GRUB on some systems. I don't use Ubuntu (this isn't why), but it
  affects Mint and Pop!, for example. Wish they'd base their distributions on
  Debian testing, but I understand why it's easier to customize an Ubuntu
  release than a Debian non-release.

* etc/doas.conf
  From BSDland, doas is a minimal alternative to sudo which by its simplicity
  is much easier to audit for security flaws. My config gives users in group
  sudo access to become root using doas, and preserves the same X11 related
  environment variables sudo would.

* etc_apt_apt.conf.d/99aptitude-colors
  Aptitude looks like ass. It only takes a couple of little changes to improve
  it dramatically. I've made them. I've also (almost completely) documented
  the defaults.

* local_bin/imgurbash2
  I didn't write this, from github: local_bin/imgurbash2
  Script that, given an image file as an argument, will upload it to imgur and
  return the URL (and deletion hash.) Handy!
